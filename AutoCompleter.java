package autocomplete;

import java.util.List;

/**
 * Abstract interface that auto completes the words typed
 */
public interface AutoCompleter {
    /**
     * abstract method initializes the list of words
     * @param filename the name of the file used.
     */
    void initialize(String filename);
    /**
     * The abstract method that keeps relevant words
     * @param prefix the prefix for sorting
     * @return the list of sorted words
     */
    List<String> allThatBeginsWith(String prefix);

    /**
     * takes the last operation time stub
     * @return time
     */
    long getLastOperationTime();
}

