package autocomplete;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * the main method
 */
public class Lab9 extends Application {

    //Width of FXML
    private static final int WIDTH = 600;

    //Height of FXML
    private static final int HEIGHT = 400;
    /**
     * the initializer of the program
     * @param primaryStage the location of the gui
     * @throws Exception throws the exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader fxmlLoader = new FXMLLoader();
        Parent root = fxmlLoader.load(getClass().getResource("Lab9Controller.fxml").openStream());
        primaryStage.setTitle("Auto Complete");
        primaryStage.setScene(new Scene(root, WIDTH, HEIGHT));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
