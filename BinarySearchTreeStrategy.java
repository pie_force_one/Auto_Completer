package autocomplete;

import java.util.List;

/**
 * TODO!!
 * Using a pre order binary search tree
 * to make a quicker auto completer
 */

public class BinarySearchTreeStrategy implements AutoCompleter {
    /**
     * The time it takes for the program to run
     */
    private long timeTaken = 0;
    /**
     * initializes the list of words
     * @param filename the name of the file used.
     */
    public void initialize(String filename){

    }

    /**
     * The sorter that keeps relevant words
     * @param prefix the prefix for sorting
     * @return the list of sorted words
     */
    public List<String> allThatBeginsWith(String prefix){
        return null;
    }

    /**
     * takes the last operation time
     * @return time
     */
    public long getLastOperationTime(){
        return timeTaken;
    }
}
