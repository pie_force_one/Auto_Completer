/*
 * CS2852 - 021
 * Spring 2018
 * Lab 9: Autocomplete Revisited
 * Name: Ryan Petter
 * Date: May 9, 2018
 */

package petterrj;


import javafx.scene.control.Alert;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Using a binary search tree is our second implementation strategy
 * to make a quicker auto completer
 */
public class BSTStrategy {
    BSTString tree = new BSTString();
    private List<String> narrowedWords = new ArrayList<>();
    String data;

    /**
     * The time it takes for the program to run
     */
    public long timeTaken = 0;

    /**
     * initializes the list of words
     *
     * @param filename the name of the file used.
     */
    public void initialize(String filename) {
        try {
            Scanner in = new Scanner(new File(filename));
            if (filename.endsWith(".txt")) {
                while (in.hasNext()) {
                    data = in.next();
                    tree.insertNode(data);
                }
            } else if (filename.endsWith(".csv")) {
                while (in.hasNext()) {
                    String tempString = in.nextLine();
                    data = (tempString.substring(tempString.indexOf(",") + 1));
                    tree.insertNode(data);
                }
            }
        } catch (FileNotFoundException e) {
            Alert fileNotFoundAlert = new Alert(Alert.AlertType.ERROR,
                    "The file was not found.");
            fileNotFoundAlert.show();
        }

        System.out.println("init complete");
    } // End of main method


    /**
     * The sorter that keeps relevant words
     *
     * @param prefix the prefix for sorting
     * @return the list of sorted words
     */
    public List<String> allThatBeginsWith(String prefix) {
        System.out.println("into allThatBeginsWith complete");
        timeTaken = System.nanoTime();
        if (data.startsWith(prefix)) {
            narrowedWords.add(data);
        }
        timeTaken = System.nanoTime() - timeTaken;
        return narrowedWords;
    }


    /**
     * takes the last operation time
     *
     * @return time
     */
    public long getLastOperationTime() {
        return timeTaken;
    }
}

