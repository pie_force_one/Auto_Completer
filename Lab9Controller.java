package autocomplete;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.text.DecimalFormat;
import java.util.List;

/**
 * The controller for the dictionary
 */
public class Lab9Controller {
    @FXML
    private Label time;
    @FXML
    private Label numberOfMatches;
    @FXML
    private RadioMenuItem arrayListIndexStrategy;
    @FXML
    private RadioMenuItem arrayListForEachStrategy;
    @FXML
    private RadioMenuItem linkedListIndexStrategy;
    @FXML
    private RadioMenuItem linkedListForEachStrategy;
    @FXML
    private RadioMenuItem binarySearchStrategy;
    @FXML
    private ToggleGroup strategy;
    @FXML
    private TextField userInput;
    @FXML
    private TextArea dictionarySearch;

    private FileChooser fileChooser = new FileChooser();
    private IndexArrayStrategy indexArrayStrategy = new IndexArrayStrategy();
    private IndexLinkedStrategy indexLinkedStrategy = new IndexLinkedStrategy();
    private ForEachArrayStrategy forEachArrayStrategy = new ForEachArrayStrategy();
    private ForEachLinkedStrategy forEachLinkedStrategy = new ForEachLinkedStrategy();
    private BinarySearchTreeStrategy binarySearchTreeStrategy = new BinarySearchTreeStrategy();
    private static final long HOUR = 3600000000000L;
    private static final long MINUTE = 60000000000L;
    private static final double SECOND = 1000000000;
    private static final double MILLISECOND = 1000000;
    private static final double MICROSECOND = 1000;

    /**
     * Opens the file
     *
     */
    public void open() {
        if (strategy.getSelectedToggle() != null) {
            try {
                File file = fileChooser.showOpenDialog(null);
                String filename = file.toString();
                if (strategy.getSelectedToggle() == arrayListIndexStrategy) {
                    indexArrayStrategy.initialize(filename);
                } else if (strategy.getSelectedToggle() == linkedListIndexStrategy) {
                    indexLinkedStrategy.initialize(filename);
                } else if (strategy.getSelectedToggle() == arrayListForEachStrategy) {
                    forEachArrayStrategy.initialize(filename);
                } else if (strategy.getSelectedToggle() == linkedListForEachStrategy) {
                    forEachLinkedStrategy.initialize(filename);
                } else if (strategy.getSelectedToggle() == binarySearchStrategy) {
                    binarySearchTreeStrategy.initialize(filename);
                }

                dictionarySearch.setText("Dictionary updated");
            } catch (NullPointerException e) {
                Alert nullPointerAlert = new Alert(Alert.AlertType.ERROR,
                        "No file was chosen.");
                nullPointerAlert.show();
            }
            userInput.setDisable(false);
        } else {
            Alert noStrategySelectedAlert = new Alert(Alert.AlertType.ERROR,
                    "You did not select a strategy.");
            noStrategySelectedAlert.show();
        }

    }

    /**
     * Closes the file
     *
     */
    public void close() {
        Platform.exit();
    }

    /**
     * help with operating program
     *
     */
    public void help() {
        Stage popUpWindow = new Stage();
        popUpWindow.setTitle("help?");
        Label label1 = new Label("First click on strategy, then " +
                "word \n file you would like to search");
        Button button1 = new Button("Close");
        button1.setOnAction(e -> popUpWindow.close());
        VBox layout = new VBox(10);
        layout.getChildren().addAll(label1, button1);
        layout.setAlignment(Pos.CENTER);
        Scene scene1 = new Scene(layout, 300, 200);
        popUpWindow.setScene(scene1);
        popUpWindow.showAndWait();

    }

    /**
     * updates the list
     *
     */
    public void update() {
        List<String> resultList = null;
        StringBuilder list = new StringBuilder();
        dictionarySearch.clear();
        if (strategy.getSelectedToggle() == arrayListIndexStrategy) {
            resultList = indexArrayStrategy.allThatBeginsWith(userInput.getText());
            timeCalculator(indexArrayStrategy.getLastOperationTime());
        } else if (strategy.getSelectedToggle() == linkedListIndexStrategy) {
            resultList = indexLinkedStrategy.allThatBeginsWith(userInput.getText());
            timeCalculator(indexLinkedStrategy.getLastOperationTime());
        } else if (strategy.getSelectedToggle() == arrayListForEachStrategy) {
            resultList = forEachArrayStrategy.allThatBeginsWith(userInput.getText());
            timeCalculator(forEachArrayStrategy.getLastOperationTime());
        } else if (strategy.getSelectedToggle() == linkedListForEachStrategy) {
            resultList = forEachLinkedStrategy.allThatBeginsWith(userInput.getText());
            timeCalculator(forEachLinkedStrategy.getLastOperationTime());
        } else if (strategy.getSelectedToggle() == binarySearchStrategy) {
            resultList = binarySearchTreeStrategy.allThatBeginsWith(userInput.getText());
            timeCalculator(binarySearchTreeStrategy.getLastOperationTime());
        }
        assert null != resultList;
        for (String string : resultList) {
            list.append(string)
                    .append("\n");
        }
        dictionarySearch.setText(list.toString());
        numberOfMatches.setText("Matches Found: " + String.valueOf(resultList.size()));
    }

    /*
     * Prints out the calculated time at the bottom of the screen
     */
    private void timeCalculator(long estimatedTime){
        DecimalFormat ref = new DecimalFormat("00.000");
        DecimalFormat ref1 = new DecimalFormat("00");
        if(estimatedTime<MICROSECOND){
            String resultNanoseconds = ref.format((double)estimatedTime);
            time.setText(resultNanoseconds + "nanoseconds");
        } else if(estimatedTime<MILLISECOND){
            double microseconds = (double)estimatedTime/MICROSECOND;
            String resultMicroseconds = ref.format(microseconds);
            time.setText(resultMicroseconds + "microseconds");
        } else if(estimatedTime<SECOND){
            double milliseconds = (double)estimatedTime/MILLISECOND;
            String resultMilliseconds = ref.format(milliseconds);
            time.setText(resultMilliseconds + "milliseconds");
        } else {
            long hours = estimatedTime/HOUR;
            long leftoverMinutes = estimatedTime % HOUR;
            long minutes = leftoverMinutes/MINUTE;
            long leftOverSeconds = leftoverMinutes % MINUTE;
            double seconds = leftOverSeconds/SECOND;
            String resultSeconds = ref.format(seconds);
            String resultMinutes = ref1.format(minutes);
            String resultHours = ref1.format(hours);
            time.setText("Estimated Run Time: " + resultHours + ":" + resultMinutes + ":" +
                         resultSeconds);
        }
    }
}
