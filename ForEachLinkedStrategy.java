package autocomplete;

import javafx.scene.control.Alert;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Using a for each loop as an linked strategy
 */
public class ForEachLinkedStrategy implements AutoCompleter {
    private List<String> words = new LinkedList<>();
    private List<String> narrowedWords = new LinkedList<>();
    /**
     * The time it takes for the program to run
     */
    public long timeTaken = 0;
    /**
     * initializes the list of words
     * @param filename the name of the file used.
     */
    public void initialize(String filename){
        try(Scanner in = new Scanner(new File(filename))){
            if(filename.endsWith(".txt")){
                words.clear();
                while (in.hasNext()){
                    words.add(in.nextLine());
                }
            } else if(filename.endsWith(".csv")){
                words.clear();
                while (in.hasNext()){
                    String tempString = in.nextLine();
                    words.add(tempString.substring(tempString.indexOf(",")+1));
                }
            }
        } catch (FileNotFoundException e){
            Alert fileNotFoundAlert = new Alert(Alert.AlertType.ERROR,
                    "The file was not found.");
            fileNotFoundAlert.show();
        }
    }
    /**
     * The sorter that keeps relevant words
     * @param prefix the prefix for sorting
     * @return the list of sorted words
     */
    public List<String> allThatBeginsWith(String prefix){
        timeTaken = System.nanoTime();
        narrowedWords.clear();
        for (String word: words) {
            if(word.startsWith(prefix)){
                narrowedWords.add(word);
            }
        }
        timeTaken = System.nanoTime() - timeTaken;
        return narrowedWords;
    }
    /**
     * takes the last operation time
     * @return time
     */
    public long getLastOperationTime(){
        return timeTaken;
    }
}
